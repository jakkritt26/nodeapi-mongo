var apiMiddleware = (req, res, next) =>{
    let token = req.headers.authorization;
    //console.log(req.headers)
    if(token) next();
    else res.status(200).send({"message":"authorization : Invalid"});
}

module.exports = apiMiddleware;